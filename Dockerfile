FROM python:3.7
COPY . /app
WORKDIR /app
RUN pip install .
ENTRYPOINT ["python"]
CMD ["vexpress/cmd/run.py"]
