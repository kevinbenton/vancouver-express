# vancouver-express

A URL blacklisting and lookup service.


# running tests

all tests are automated and run with the `tox` command in the base directory.

It will run the following jobs, each in their own venvs with all dependencies automatically installed:

pep8 - code style and linting
py3 - unit tests
functional - functional tests
coverage - generate code coverage reports from above

NOTE that the functional tests do start up a local webserver so a local firewall may
interfere with connections and cause failures.


# Environmental vars to control the service

The service supports two backends

* sqlite (in-memory or on-disk)
* etcd

These can be controlled with the following environmental vars:

`VEXPRESS_BACKEND_TYPE` to select backend. Must be `sqlite_in_memory`, `sqlite`, or `etcd`.
Default is sqlite in memory.

`VEXPRESS_SQLITE_PATH` to set the path to the sqlite.db for file backed sqlite.

`VEXPRESS_ETCD_HOST` and `VEXPRESS_ETCD_PORT` to set the host and port number for the etcd
server. Defaults to localhost:2379

# Building and Running with Docker

## Build

```
docker build -t vexpress:latest .
```

## Run

listening on port 5000 with sqlite_in_memory backend
```
docker run -e VEXPRESS_BACKEND_TYPE=sqlite_in_memory -p 5000:15000 vexpress:latest
```

## Check service

```
$ curl -v localhost:5000/urlinfo/1/google.com
*   Trying 127.0.0.1...
* TCP_NODELAY set
* Connected to localhost (127.0.0.1) port 5000 (#0)
> GET /urlinfo/1/google.com HTTP/1.1
> Host: localhost:5000
> User-Agent: curl/7.54.0
> Accept: */*
>
* HTTP 1.0, assume close after body
< HTTP/1.0 200 OK
< Content-Type: text/html; charset=utf-8
< Content-Length: 0
< Cache-Control: max-age=30, public
< Server: Werkzeug/0.16.0 Python/3.7.4
< Date: Fri, 27 Sep 2019 17:33:53 GMT
<
* Closing connection 0
```

# API requirements

Request format:

    GET /urlinfo/1/{hostname_and_port}/{original_path_and_query_string}

   Example - /urlinfo/1/cisco.com:443/hello?name=Kevin


Responses:
    All URL info requests are driven by response codes only for fast serialization and parsing:
      200 - URL is not blocked
      400 - lookup URL is malformed (unexpected hostname:port)
      403 - Access is denied to the requested URL
      429 - Processing queue is overloaded. Backoff please.
      500 - Server hit an error
      504 - Timed out waiting on backend task to complete

    Response headers for 200 and 403 include cache directives to allow a front-end caching layer
    to reduce load on the server.

# Payload format


## Create
Blocking policies can be POSTed to the `/bulk_policies/` endpoint in the following json format.

```
[
    {
        "uuid": "a5ca4c16-9fb9-4fd4-8d93-181355038fd0",
        "url": {
            "hostname": "example1.org",
            "path": "",
            "port": "80",
            "query_string": ""
        },
        "match_criteria": {
            "exact_match": true
        }
    },
    {
        "uuid": "a90a65ff-9123-4a24-b675-0de5495a2c47",
        "url": {
            "hostname": "example1.org",
            "path": "",
            "port": "80",
            "query_string": "?test=1&test3"
        },
        "match_criteria": {
            "exact_match": true
        }
    },
    {
        "uuid": "2dfe4fe1-0f4f-49dc-abba-b66f14f0f651",
        "url": {
            "hostname": "example.org",
            "path": "nested/dir",
            "port": "80",
            "query_string": "?test=1&test3"
        },
        "match_criteria": {
            "exact_match": true
        }
    },
    {
        "uuid": "7fd38f10-f267-469c-9b09-cdbadd2b7ca2",
        "url": {
            "hostname": "example.org",
            "path": "nested/dir",
            "port": "80",
            "query_string": "&"
        },
        "match_criteria": {
            "exact_match": true
        }
    },
    {
        "uuid": "cae2bb6c-e0e2-4c07-b030-1c005d0bc6c0",
        "url": {
            "hostname": "example.org",
            "path": "nested/dir",
            "port": "80",
            "query_string": "jjs"
        },
        "match_criteria": {
            "exact_match": true
        }
    },
    {
        "uuid": "d0a29fee-865e-4ef7-b171-15073a99aa1b",
        "url": {
            "hostname": "example.org",
            "path": "nested/dir",
            "port": "80",
            "query_string": "/dirs/in/query/"
        },
        "match_criteria": {
            "exact_match": true
        }
    }
]
```

## Get

Curently a GET of all policies is all that is supported to /bulk_policies/`

## Update

A policy can be replaced by using its UUID and performing a PUT to `/bulk_policies/<policy-uuid>`.
A POST to `/bulk_policies/` with the same UUID will replace the policy under that same UUID as well.

## Delete

Policies can be DELETEd by sending a delete to `/bulk_policies/<policy-uuid>`.



# Architecutre of Program

There in an httphandler frontend that is designed to be multi-threaded.
This communicates with a single backend worker thread. This is to accomadate
non-thread-safe backends (e.g. sqlite).

The backend is responsible for doing the lookups and storage. This program
currently supports two backeds: sqlite and etcd.

The key data model is the BlockPolicy which is comprised of a URL, a UUID,
and a MatchPolicy. The MatchPolicy was put in place to allow wildcarding
on the URL
(e.g. block any requests that match the domain, regardless of path and query).
This isn't implemented yet so the only supported MatchPolicy is exact_match.

## etcd backend

Policies are stored each in their own key with a json-serialized version of
themselves under /block_policies/ in etcd.
Additionally, a hashed version of the URL blocked by the policy is stored under
'/hash_lookups/' so checking for a block is accomplished by a single key lookup.

This backend supports multiple frontends assuming the same etcd cluster is used
for all of them.


## sqlite backend

This backend just has a single table schema right now to store the block policies
and block checks are expensively implemented with a select statement matching
all criteria.


# Work Checklist


## Complete

* checklist
* tox testing environments for both functional and unit test jobs
* Implement abstracted backend API to add and lookup blacklisted URLs
* Implement backends
    * sqlite backend for simple disk-basked single node env
    * etcd backend for clustered approach
* client constructors for backend
* Implement front-end HTTP API
    * Implement lookup portion
    * Implement create/update/delete portion
* usage docs with API samples
* containerize


## Incomplete
* LRU cache in front of lookup portion


## Limitations

* bulk sql inserts not implemented (non-atomic behavior)
* no DB migration strategy

* etcd connection recovery, authentication, encyrption, not implemented
