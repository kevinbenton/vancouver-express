import logging
import os
import queue
import sqlite3
import threading

import etcd3

from vexpress import httphandler
from vexpress.backend import etcd
from vexpress.backend import sqlite
from vexpress.backend import worker

LOG = logging.getLogger()


def _get_backend():
    backend_type = os.environ.get('VEXPRESS_BACKEND_TYPE', 'sqlite_in_memory')
    if backend_type in ('sqlite_in_memory', 'sqlite'):
        if backend_type == 'sqlite_in_memory':
            path = ":memory:"
        else:
            path = os.environ.get('VEXPRESS_SQLITE_PATH')
        LOG.info("Using sqlite backend with connection to %s", path)
        conn = sqlite3.connect(path)
        backend = sqlite.SqliteBackend(conn)
        backend.initialize_schema()
        return backend
    if backend_type == 'etcd':
        client = etcd3.client(host=os.environ.get('VEXPRESS_ETCD_HOST', 'localhost'),
                              port=int(os.environ.get('VEXPRESS_ETCD_PORT', '2379')))
        key_prefix = os.environ.get('VEXPRESS_ETCD_KEY_PREFIX', 'vexpress')
        LOG.info("Using etcd client %s with key prefix %s", client, key_prefix)
        return etcd.EtcdBackend(client, key_prefix)
    raise RuntimeError("Unrecognized backend type %s" % backend_type)


def prepare_workers():
    # MAX work queue size to induce backpressue
    work_queue = queue.Queue(maxsize=1000)
    req_handler = httphandler.FlaskHandler(work_queue)
    bworker = worker.BackendWorker(work_queue, _get_backend())
    return bworker, req_handler


def run():
    logging.basicConfig(level=logging.DEBUG)
    bworker, req_handler = prepare_workers()
    threading.Thread(target=req_handler.run, daemon=True).start()
    bworker.do_work()
