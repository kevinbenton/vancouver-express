import queue
import uuid

import flask

from vexpress.backend import api
from vexpress.backend import jobs


# maximume time a look up can take before we give up an issue a 504
# error to the caller
MAX_RESPONSE_TIME = 2

# how many seconds to put in response header
EXTERNAL_CACHE_AGE = 30


class FlaskHandler():
    """HTTP request handler. A separate worker is required to drain the queue."""

    def __init__(self, work_queue):
        # work queue is a thread-safe queue where we put our jobs
        self.work_queue = work_queue
        self.flask_app = flask.Flask(__name__)
        self.flask_app.add_url_rule(
            '/bulk_policies/',
            'handle_bulk_post',
            self.handle_bulk_post,
            methods=["POST"]
        )
        self.flask_app.add_url_rule(
            '/bulk_policies/',
            'handle_bulk_get',
            self.handle_bulk_get,
            methods=["GET"]
        )
        self.flask_app.add_url_rule(
            '/bulk_policies/<policy_id>',
            'handle_policy_put',
            self.handle_policy_put,
            methods=["PUT"]
        )
        self.flask_app.add_url_rule(
            '/bulk_policies/<policy_id>',
            'handle_policy_delete',
            self.handle_policy_delete,
            methods=["DELETE"]
        )
        self.flask_app.route('/', defaults={'path': ''})(self.fallback)
        self.flask_app.route('/<path:path>')(self.fallback)

    def fallback(self, path):
        if path.startswith('urlinfo/1/'):
            return self.handle_url_lookup(path.split('urlinfo/1/', 1)[1])
        flask.abort(404, 'You hit path: %s' % path)
        return None

    def run(self):
        """Blocking method that runs an HTTP server."""
        self.flask_app.run(host='0.0.0.0', port=15000)

    def _submit_and_wait_for_job(self, job):
        # we block in the admin API
        self.work_queue.put(job)
        job.is_processed.wait()
        if job.error is not None:
            flask.abort(500, job.error)
        return flask.jsonify({'result': job.result})

    def handle_bulk_post(self):
        policies = []
        for item in flask.request.get_json():
            # auto generate UUID for policy
            if 'uuid' not in item:
                item['uuid'] = str(uuid.uuid4())
            policies.append(api.BlockPolicy.from_dict(item))
        job = jobs.PutPoliciesRequest(policies)
        return self._submit_and_wait_for_job(job)

    def handle_policy_put(self, policy_uuid):
        payload = flask.request.get_json()
        payload['uuid'] = policy_uuid
        job = jobs.PutPoliciesRequest([api.BlockPolicy.from_dict(payload)])
        return self._submit_and_wait_for_job(job)

    def handle_policy_delete(self, policy_id):
        job = jobs.DeletePoliciesRequest([policy_id])
        return self._submit_and_wait_for_job(job)

    def handle_bulk_get(self):
        job = jobs.GetPoliciesRequest()
        return self._submit_and_wait_for_job(job)

    def handle_url_lookup(self, path):
        host_port, *trailing = path.split('/')
        remainder = '/'.join(trailing)
        query_string = flask.request.query_string.decode('utf-8')
        urlobj = validate_and_make_url_object(host_port, remainder, query_string)
        if not urlobj:
            # bad request
            flask.abort(400, "Malformed lookup URL")

        if not self.check_url_blocked(urlobj):
            # 200 is OK
            resp = flask.make_response('', 200)
        else:
            # 403 means Forbidden
            resp = flask.make_response('', 403)
        # allow caching of these results at HTTP cache layers as well
        resp.cache_control.max_age = EXTERNAL_CACHE_AGE
        resp.cache_control.public = True
        return resp

    def check_url_blocked(self, urlobj):
        work_item = jobs.LookupRequest(urlobj)
        try:
            self.work_queue.put_nowait(work_item)
        except queue.Full:
            # queue is full, issue Too Many Requests error
            flask.abort(429)

        if not work_item.is_processed.wait(MAX_RESPONSE_TIME):
            # took too long to respond, issue Gateway Timeout error
            flask.abort(504)
        if work_item.error is not None:
            flask.abort(500)
        # result is a boolean indicating if it's blocked
        return work_item.result


def validate_and_make_url_object(host_port, path, query_string):
    if ':' in host_port:
        try:
            host, port = host_port.split(':')
        except ValueError:
            # got too many colons
            return None
    else:
        # assume port 80 if unspecified
        host, port = host_port, "80"
    return api.URL(host, port, path, query_string)
