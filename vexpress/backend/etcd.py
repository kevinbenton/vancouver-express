import logging
import json

from vexpress.backend import api


LOG = logging.getLogger(__name__)


class EtcdBackend(api.URLBackendAPI):

    def __init__(self, etcd_client, key_pref):
        self.client = etcd_client
        # a key prefix all of our entries will be scoped under
        self.key_pref = key_pref
        self._block_dir = self._full_key('/block_policies/')
        self._hash_dir = self._full_key('/hash_to_blocking_policy/')

    def _full_key(self, key):
        # get fully qualified key name
        return "/%s%s" % (self.key_pref, key)

    def _del_key(self, key):
        return self.client.delete(key)

    @staticmethod
    def _try_decode(raw_etcd_value):
        try:
            return api.BlockPolicy.from_dict(json.loads(raw_etcd_value.decode('utf-8')))
        except (ValueError, KeyError):
            LOG.exception("Unable to decode block policy definition %s", raw_etcd_value)
            return None

    def get_block_policy(self, uuid):
        val = self.client.get(self._block_dir + uuid)[0]
        if val is None:
            raise api.PolicyNotFound(uuid)
        block_policy = self._try_decode(val)
        if not block_policy:
            # bad encoding, error
            raise api.PolicyNotFound(uuid)
        return block_policy

    def _get_existing_uuids(self, hash_key):
        val, meta = self.client.get(self._hash_dir + hash_key)
        if (val, meta) == (None, None):
            return []
        try:
            uuids = json.loads(val.decode('utf-8'))
        except ValueError:
            uuids = []
        return uuids

    def remove_uuid_from_hash(self, uuid, hash_key):
        uuids = [u for u in self._get_existing_uuids(hash_key) if u != uuid]
        if uuids:
            self.client.put(self._hash_dir + hash_key, json.dumps(uuids))
        else:
            self._del_key(self._hash_dir + hash_key)

    def add_uuid_to_hash(self, uuid, hash_key):
        # NOTE: this and delete_block_policy are not race safe between multiple
        # servers. This should be fixed with a conditional etcd put based on revision
        # number. Didn't have time, sorry :(
        uuids = self._get_existing_uuids(hash_key)
        uuids.append(uuid)
        self.client.put(self._hash_dir + hash_key, json.dumps(uuids))

    def delete_block_policy(self, uuid):
        block_policy = self.get_block_policy(uuid)
        self._del_key(self._block_dir + block_policy.uuid)
        for key in block_policy.generate_all_hash_matches():
            self.remove_uuid_from_hash(uuid, key)

    def get_block_policies(self):
        for val, _ in self.client.get_prefix(self._block_dir):
            block_policy = self._try_decode(val)
            if not block_policy:
                continue
            yield block_policy

    def is_url_blocked(self, urlobj):
        for key in urlobj.generate_all_hashes():
            # check for any of the possible hashes
            resp = self.client.get(self._hash_dir + key)
            if resp != (None, None):
                return True
        return False

    def put_block_policy(self, block_policy):
        # put main encoded def under block entries
        value = json.dumps(block_policy.to_dict())
        self.client.put(self._block_dir + block_policy.uuid, value)
        # enter all hash lookup shortcuts
        for key in block_policy.generate_all_hash_matches():
            self.add_uuid_to_hash(block_policy.uuid, key)
