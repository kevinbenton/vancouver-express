import threading


class _Job():
    def __init__(self):
        self._result = None
        self._error = None
        self.is_processed = threading.Event()

    @property
    def result(self):
        if not self.is_processed.is_set():
            raise RuntimeError("do not try to read result before is_processed event fires")
        return self._result

    @property
    def error(self):
        if not self.is_processed.is_set():
            raise RuntimeError("do not try to read error before is_processed event fires")
        return self._error

    def set_result(self, result, error):
        self._result = result
        self._error = error
        self.is_processed.set()


class LookupRequest(_Job):
    """A request for the backend worker to check if a URL is allowed."""

    def __init__(self, urlobj):
        self.urlobj = urlobj
        super().__init__()


class PutPoliciesRequest(_Job):
    """A request to put a set of a policies."""
    def __init__(self, policies):
        self.policies = policies
        super().__init__()


class DeletePoliciesRequest(_Job):
    """A request to delete a set of a policies."""
    def __init__(self, policy_uuids):
        self.policy_uuids = policy_uuids
        super().__init__()


class GetPoliciesRequest(_Job):
    """A request to list all of the policies."""
