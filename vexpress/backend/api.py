import abc
import hashlib


class URL():
    """Represents a URL. Can be checked against a blocking policy."""
    def __init__(self, hostname, port, path, query_string):
        self.hostname = hostname
        self.port = str(port)
        self.path = path
        self.query_string = query_string

    def __eq__(self, other):
        if not isinstance(other, URL):
            return False
        return all((self.hostname == other.hostname,
                    self.port == other.port,
                    self.path == other.path,
                    self.query_string == other.query_string))

    def __repr__(self):
        return "<URL(%s:%s/%s?%s>" % (self.hostname, self.port, self.path, self.query_string)

    @classmethod
    def from_dict(cls, some_dict):
        for key in ('hostname', 'path', 'port', 'query_string'):
            if key not in some_dict:
                raise KeyError("Missing required key %s" % key)
        return cls(some_dict['hostname'],
                   some_dict['port'],
                   some_dict['path'],
                   some_dict['query_string'])

    def to_dict(self):
        return {'hostname': self.hostname,
                'path': self.path,
                'port': self.port,
                'query_string': self.query_string}

    def exact_match_hash(self):
        hashobj = hashlib.sha256()
        # add in everything for an exact match
        for val in (self.hostname, self.path, self.port, self.query_string):
            hashobj.update(val.encode('utf-8'))
        return hashobj.hexdigest()

    def generate_all_hashes(self):
        return [self.exact_match_hash()]


class BlockPolicy():
    """Block policy. Comprised of a URL and a matching criteria."""
    def __init__(self, uuid, urlobj, match_criteria):
        self.uuid = str(uuid)
        self.url = urlobj
        self.match_criteria = match_criteria

    def __eq__(self, other):
        if not isinstance(other, BlockPolicy):
            return False
        return all((self.uuid == other.uuid,
                    self.url == other.url,
                    self.match_criteria == other.match_criteria))

    def __repr__(self):
        return "<BlockPolicy(uuid=%s, url=%s, match_criteria=%s)>" % (self.uuid, self.url,
                                                                      self.match_criteria)

    @classmethod
    def from_dict(cls, some_dict):
        for key in ('uuid', 'url', 'match_criteria'):
            if key not in some_dict:
                raise KeyError("Missing required key %s" % key)
        return cls(some_dict['uuid'], URL.from_dict(some_dict['url']),
                   MatchCriteria.from_dict(some_dict['match_criteria']))

    def to_dict(self):
        return {'uuid': self.uuid,
                'url': self.url.to_dict(),
                'match_criteria': self.match_criteria.to_dict()}

    def generate_all_hash_matches(self):
        if self.match_criteria.exact_match:
            return [self.url.exact_match_hash()]
        return []


class MatchCriteria():
    """Match Criteria. Determines what must match in URL for blocking."""
    def __init__(self):
        # initially we only support exact matches
        self.exact_match = True

    def __eq__(self, other):
        if not isinstance(other, MatchCriteria):
            return False
        return other.exact_match == self.exact_match

    def __repr__(self):
        return "<MatchCriteria(exact_match=%s)>" % self.exact_match

    @classmethod
    def from_dict(cls, some_dict):
        # we don't do anything yet for match criterea
        for key in ('exact_match', ):
            if key not in some_dict:
                raise KeyError("Missing required key %s" % key)
        return cls()

    def to_dict(self):
        return {'exact_match': self.exact_match}


class PolicyNotFound(Exception):
    """Raised when block policy isn't found."""


class URLBackendAPI(abc.ABC):

    """API methods that a storage backend must implement"""

    @abc.abstractmethod
    def put_block_policy(self, block_policy):
        pass

    @abc.abstractmethod
    def delete_block_policy(self, uuid):
        pass

    @abc.abstractmethod
    def get_block_policies(self):
        # Returns list of BlockPolicies
        pass

    @abc.abstractmethod
    def is_url_blocked(self, urlobj):
        # return boolean determining if URLobj blocked
        pass
