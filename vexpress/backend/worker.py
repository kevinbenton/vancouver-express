import logging

from vexpress.backend import api
from vexpress.backend import jobs

LOG = logging.getLogger()


class BackendWorker():
    """Working responsible for fulfilling requests to a backend."""

    def __init__(self, work_queue, backend):
        self.work_queue = work_queue
        self.backend = backend

    def _get_handler(self, item):
        if isinstance(item, jobs.LookupRequest):
            return self.handle_lookup_request
        if isinstance(item, jobs.PutPoliciesRequest):
            return self.handle_put_policies_request
        if isinstance(item, jobs.DeletePoliciesRequest):
            return self.handle_delete_policies_request
        if isinstance(item, jobs.GetPoliciesRequest):
            return self.handle_get_policies_request
        return None

    def do_work(self):
        while True:
            item = self.work_queue.get()
            handler = self._get_handler(item)
            if not handler:
                LOG.error("Unrecognized work item in queue: %s", item)
                continue
            try:
                handler(item)
            except Exception as error:  # pylint: disable=broad-except
                LOG.exception("Exception encountered trying to process work item")
                item.set_result(None, repr(error))

    def handle_lookup_request(self, item):
        item.set_result(self.backend.is_url_blocked(item.urlobj), None)

    def handle_put_policies_request(self, item):
        for block_policy in item.policies:
            self.backend.put_block_policy(block_policy)
        item.set_result('ok', None)

    def handle_delete_policies_request(self, item):
        count = 0
        for policy_uuid in item.policy_uuids:
            try:
                self.backend.delete_block_policy(policy_uuid)
                count += 1
            except api.PolicyNotFound:
                pass
        item.set_result("%s policies deleted" % count, None)

    def handle_get_policies_request(self, item):
        item.set_result([p.to_dict() for p in self.backend.get_block_policies()], None)
