import logging
import sqlite3

from vexpress.backend import api


LOG = logging.getLogger(__name__)


class SqliteBackend(api.URLBackendAPI):

    def __init__(self, sqlitecon):
        self.conn = sqlitecon

    def delete_block_policy(self, uuid):
        with self.conn as cur:
            if not cur.execute('delete from block_policies where block_uuid=?',
                               (uuid,)).rowcount:
                raise api.PolicyNotFound(uuid)

    def get_block_policies(self):
        with self.conn as cur:
            for row in cur.execute("select * from block_policies"):
                uuid, hname, port, path, query_str = row
                yield api.BlockPolicy(
                    uuid,
                    api.URL(hname, port, path, query_str),
                    api.MatchCriteria()
                )

    def is_url_blocked(self, urlobj):
        with self.conn as cur:
            result = cur.execute("select block_uuid from block_policies where "
                                 "hostname=? AND port=? AND path=? and query_string=?",
                                 (urlobj.hostname, urlobj.port, urlobj.path, urlobj.query_string))
            for _ in result:
                # there was a block match. return
                return True
        return False

    def put_block_policy(self, block_policy):
        insert_args = ('insert into block_policies values (?, ?, ?, ?, ?)',
                       (block_policy.uuid, block_policy.url.hostname, block_policy.url.port,
                        block_policy.url.path, block_policy.url.query_string))
        try:
            with self.conn as cur:
                cur.execute(*insert_args)
        except sqlite3.IntegrityError:
            # already exists delete and recreate
            with self.conn as cur:
                cur.execute('delete from block_policies where block_uuid=?', (block_policy.uuid,))
                cur.execute(*insert_args)

    def initialize_schema(self):
        try:
            with self.conn as cur:
                cur.execute("select * from block_policies")
                # just assume presence of table means it's ok. NO UPGRADE PATH
                return
        except sqlite3.OperationalError:
            pass
        with self.conn as cur:
            cur.execute('''create TABLE block_policies
                           (block_uuid varchar unique, hostname text,
                            port int, path text, query_string text)''')
