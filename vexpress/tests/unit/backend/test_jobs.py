import testtools
from vexpress.backend import jobs


class TestLookupRequest(testtools.TestCase):

    def test_early_read(self):
        req = jobs.LookupRequest(None)
        self.assertRaises(RuntimeError, getattr, req, 'result')
        self.assertRaises(RuntimeError, getattr, req, 'error')
        req.set_result(True, None)
        getattr(req, 'result')
        getattr(req, 'error')
