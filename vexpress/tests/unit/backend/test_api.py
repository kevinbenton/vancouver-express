import testtools
from vexpress.backend import api


class TestURL(testtools.TestCase):

    def test_repr(self):
        self.assertEqual('<URL(hostname:80/path?query_str>',
                         repr(api.URL('hostname', 80, 'path', 'query_str')))

    def test_equality(self):
        url1 = api.URL('hostname', 80, 'path', 'query_str')
        url2 = api.URL('hostname', 80, 'path', 'query_str')
        self.assertEqual(url1, url2)
        self.assertNotEqual(url1, api.BlockPolicy(None, None, None))
        self.assertNotEqual(api.BlockPolicy(None, None, None), url1)
