import uuid

import testtools

from vexpress.backend import api


class BackendAPITestCases(testtools.TestCase):
    """Mixin class of test cases to use with backends."""

    def test_get_block_policies(self):
        url = api.URL('hostname.com', 80, '/somepath', 'query_string=40')
        block_policy = api.BlockPolicy(uuid.uuid4(), url, api.MatchCriteria())
        self.backend.put_block_policy(block_policy)
        self.assertEqual([block_policy], list(self.backend.get_block_policies()))

    def test_put_block_policy(self):
        url = api.URL('hostname.com', 80, '/somepath', 'query_string=40')
        block_policy = api.BlockPolicy(uuid.uuid4(), url, api.MatchCriteria())
        self.backend.put_block_policy(block_policy)
        block_policy.url.hostname = 'newhost.com'
        self.backend.put_block_policy(block_policy)
        self.assertEqual([block_policy], list(self.backend.get_block_policies()))
        self.assertEqual('newhost.com', list(self.backend.get_block_policies())[0].url.hostname)

    def test_is_url_blocked(self):
        url = api.URL('hostname.com', 80, '/somepath', 'query_string=40')
        block_policy = api.BlockPolicy(uuid.uuid4(), url, api.MatchCriteria())
        self.backend.put_block_policy(block_policy)
        self.assertTrue(self.backend.is_url_blocked(url))
        url.query_string += "2"
        self.assertFalse(self.backend.is_url_blocked(url))

    def test_delete_block_policy_non_existent(self):
        self.assertRaises(api.PolicyNotFound, self.backend.delete_block_policy, "bad_uuid")

    def test_delete_block_policy_exists(self):
        url = api.URL('hostname.com', 80, '/somepath', 'query_string=40')
        block_policy = api.BlockPolicy(uuid.uuid4(), url, api.MatchCriteria())
        self.backend.put_block_policy(block_policy)
        # first should work
        self.backend.delete_block_policy(block_policy.uuid)
        # second should fail
        self.assertRaises(api.PolicyNotFound,
                          self.backend.delete_block_policy, block_policy.uuid)

    def test_dup_url_diff_policies(self):
        url = api.URL('hostname.com', 80, '/somepath', 'query_string=40')
        block_policy1 = api.BlockPolicy(uuid.uuid4(), url, api.MatchCriteria())
        block_policy2 = api.BlockPolicy(uuid.uuid4(), url, api.MatchCriteria())
        self.backend.put_block_policy(block_policy1)
        self.backend.put_block_policy(block_policy2)
        self.assertTrue(self.backend.is_url_blocked(url))
        self.backend.delete_block_policy(block_policy1.uuid)
        self.assertTrue(self.backend.is_url_blocked(url))
        self.backend.delete_block_policy(block_policy2.uuid)
        self.assertFalse(self.backend.is_url_blocked(url))
