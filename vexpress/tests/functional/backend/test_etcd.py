import uuid

import etcd3

from vexpress.backend import etcd
from vexpress.tests.functional.backend import common_testcases


class TestEtcdBackend(common_testcases.BackendAPITestCases):

    def setUp(self):
        # etcd server must be running locally in dev environment for this to work
        client = etcd3.client()
        rand_prefix = str(uuid.uuid4())[0:8]
        # ensure it's clean before test starts
        client.delete_prefix('/%s' % rand_prefix)
        # clean when it ends
        self.addCleanup(client.delete_prefix, '/%s' % rand_prefix)

        self.backend = etcd.EtcdBackend(client, rand_prefix)
        super().setUp()

    def test_bad_etcd_values(self):
        # pylint: disable=protected-access
        self.backend.client.put(self.backend._block_dir + 'test_bad_val', '34532}')
        self.assertEqual([], list(self.backend.get_block_policies()))
