import sqlite3

from vexpress.backend import sqlite
from vexpress.tests.functional.backend import common_testcases


class TestSqliteBackend(common_testcases.BackendAPITestCases):

    def setUp(self):
        conn = sqlite3.connect(':memory:')
        self.backend = sqlite.SqliteBackend(conn)
        self.backend.initialize_schema()
        super().setUp()
