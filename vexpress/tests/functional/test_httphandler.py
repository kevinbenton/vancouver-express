import multiprocessing
import time
import uuid

import requests
import testtools

from vexpress.cmd import cli
from vexpress.backend import api


class TestHttpHandler(testtools.TestCase):

    def setUp(self):
        self._serv_proc = multiprocessing.Process(target=cli.run)
        self.addCleanup(self._serv_proc.terminate)
        self._serv_proc.start()
        for _ in range(100):
            # wait for flask to start
            try:
                if requests.get("http://localhost:15000/foo").status_code == 404:
                    break
            except requests.exceptions.ConnectionError:
                time.sleep(0.05)
        else:
            raise Exception("http handler didn't start")
        super().setUp()

    @staticmethod
    def get_sample_policies():
        policies = []
        hostnames = ['example1.org', 'sub.example1.org', 'example.org']
        paths = ['', 'basedir', '/anotherdir', 'nested/dir']
        query_strs = ['', '?test=1&test3', '&', 'jjs', '/dirs/in/query/']
        for hostname in hostnames:
            for path in paths:
                for query in query_strs:
                    policies.append(
                        api.BlockPolicy(uuid=uuid.uuid4(),
                                        urlobj=api.URL(hostname, 80, path, query),
                                        match_criteria=api.MatchCriteria())
                    )
        return policies

    def test_bulk_post(self):
        policies = self.get_sample_policies()
        dicts = [p.to_dict() for p in policies]
        self.assertEqual(
            {'result': 'ok'},
            requests.post("http://localhost:15000/bulk_policies", json=dicts).json()
        )
        self.assertEqual(
            len(dicts),
            len(requests.get('http://localhost:15000/bulk_policies').json()['result'])
        )
        banned = [
            "http://localhost:15000/urlinfo/1/example1.org/",
            "http://localhost:15000/urlinfo/1/example1.org:80/",
            "http://localhost:15000/urlinfo/1/example.org:80/",
            "http://localhost:15000/urlinfo/1/sub.example1.org:80/",
            "http://localhost:15000/urlinfo/1/sub.example1.org:80/?&",
            "http://localhost:15000/urlinfo/1/sub.example1.org:80/??test=1&test3",
            "http://localhost:15000/urlinfo/1/sub.example1.org:80/nested/dir",
            "http://localhost:15000/urlinfo/1/sub.example1.org:80/nested/dir?jjs",
            "http://localhost:15000/urlinfo/1/sub.example1.org:80/nested/dir?/dirs/in/query/"
        ]
        for bad in banned:
            self.assertEqual(
                403, requests.get(bad).status_code, "unexpected pass on %s" % bad
            )
        accepted = [
            "http://localhost:15000/urlinfo/1/example1.org/f",
            "http://localhost:15000/urlinfo/1/example1.org:80/f",
            "http://localhost:15000/urlinfo/1/example.org:80/f",
            "http://localhost:15000/urlinfo/1/sub.example1.org:80/f",
            "http://localhost:15000/urlinfo/1/sub.example1.org:80/?f&",
            "http://localhost:15000/urlinfo/1/sub.example1.org:80/??test=f&test3",
            "http://localhost:15000/urlinfo/1/sub.example1.org:80/nested/dirf",
            "http://localhost:15000/urlinfo/1/sub.example1.org:80/f/nested/dir?jjs",
            "http://localhost:15000/urlinfo/1/sub.example1.org:80/f/nested/dir?/dirs/in/query/"
        ]
        for good in accepted:
            self.assertEqual(
                200, requests.get(good).status_code, "unexptected fail on %s" % good
            )
        # delete them all
        for policy in policies:
            self.assertEqual(
                "1 policies deleted",
                (requests.delete('http://localhost:15000/bulk_policies/%s' % policy.uuid).
                 json()['result'])
            )
        self.assertEqual(
            [],
            requests.get('http://localhost:15000/bulk_policies').json()['result']
        )
        # now make sure the previous ones work
        for good in banned:
            self.assertEqual(
                200, requests.get(good).status_code, "unexptected fail on %s" % good
            )
