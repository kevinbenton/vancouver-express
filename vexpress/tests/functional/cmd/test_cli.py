import testtools
from vexpress.cmd import cli
from vexpress.backend import worker
from vexpress import httphandler


class TestCli(testtools.TestCase):
    def test_prepare_workers(self):
        backworker, flaskhandler = cli.prepare_workers()
        self.assertIsInstance(backworker, worker.BackendWorker)
        self.assertIsInstance(flaskhandler, httphandler.FlaskHandler)
